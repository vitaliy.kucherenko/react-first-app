import React from 'react'
import './App.css';
import Body from "./components/body/body";
import Navbar from "./components/navbar/navbar";
import Footer from "./components/footer/footer";

const App = () => {
    return (
        <div className='app-wrapper'>
            <Navbar/>
            <Body />
            <Footer/>
        </div>
    );
}

export default App;
