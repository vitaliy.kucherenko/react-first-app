const SEND_MESSAGE = 'send_message';
const UPDATE_NEW_MESSAGE_TEXT = 'update-new-message-text';

let initialState = {
    dialogs: [
        {id: 1, name: 'Vitaliy'},
        {id: 2, name: 'Sasha'},
        {id: 3, name: 'Andrey'},
        {id: 4, name: 'Misha'},
        {id: 5, name: 'anna'},
        {id: 6, name: 'Inna'},
    ],
    messages: [
        {id: 1, message: 'hi 1'},
        {id: 2, message: 'hi 2'},
        {id: 3, message: 'hi 3'},
        {id: 4, message: 'hi 4'},
    ],
    newMessageText: "send message"
}

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_NEW_MESSAGE_TEXT:
            return {
                ...state,
                newMessageText: action.newText
            }
        case SEND_MESSAGE:
            let text = state.newMessageText;
            return {
                ...state,
                newMessageText: '',
                messages: [
                    ...state.messages,
                    {
                        id: state.messages[state.messages.length - 1].id + 1,
                        message: text
                    }
                ]
            }
        default:
            return state
    }

}
export const sendMessageCreator = () => ({type: SEND_MESSAGE})
export const updateMessageTextCreator = (text) => {
    return {type: UPDATE_NEW_MESSAGE_TEXT, newText: text}
}
export default dialogsReducer;