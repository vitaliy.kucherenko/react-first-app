import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";

const ADD_POST = 'add-post';
const SEND_MESSAGE = 'send_message';
const UPDATE_NEW_POST_TEXT = 'update-new-post-text';
const UPDATE_NEW_MESSAGE_TEXT = 'update-new-message-text';

let store = {
    _state: {
        profilePage: {
            posts: [
                {id: 1, message: '1'},
                {id: 2, message: '2'},
                {id: 3, message: '3'},
            ],
            text_post: 'add new post',
        },
        dialogsPage: {
            dialogs: [
                {id: 1, name: 'Vitaliy'},
                {id: 2, name: 'Sasha'},
                {id: 3, name: 'Andrey'},
                {id: 4, name: 'Misha'},
                {id: 5, name: 'anna'},
                {id: 6, name: 'Inna'},
            ],
            messages: [
                {id: 1, message: 'hi 1'},
                {id: 2, message: 'hi 2'},
                {id: 3, message: 'hi 3'},
                {id: 4, message: 'hi 4'},
            ],
            newMessageText: ""
        }
    },
    getState() {
        return this._state
    },
    _callSubscriber() {
    },
    subscriber(observer) {
        this._callSubscriber = observer
    },
    dispatch(action) {

        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._callSubscriber(this._state);

    }
}

export default store
window.store = store