const ADD_POST = 'add-post';
const UPDATE_NEW_POST_TEXT = 'update-new-post-text';

let initialState = {
    posts: [
        {id: 1, message: '1'},
        {id: 2, message: '2'},
        {id: 3, message: '3'},
    ],
    text_post: 'add new post',
}
const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            return {
                ...state,
                text_post: '',
                posts: [
                    ...state.posts,
                    {
                        id: state.posts[state.posts.length - 1].id + 1,
                        message: state.text_post
                    }
                ]
            }

        case UPDATE_NEW_POST_TEXT:
            return {
                ...state,
                text_post: action.newText
            }
        default:
            return state
    }
}
export const addPostActionCreator = () => ({type: ADD_POST})
export const updateNewPostTextActionCreator = (text) => {
    return {type: UPDATE_NEW_POST_TEXT, newText: text}
}
export default profileReducer;