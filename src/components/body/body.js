import React from 'react'
import Profile from "./profile/profile";
import {BrowserRouter, Route} from "react-router-dom";
import Sidebar from "../sidebar/sidebar";
import DialogsContainer from "./message/dialogsContainer";

const Body = (props) => {
    return (
        <BrowserRouter>
            <Sidebar/>
            <div className='body-base'>
                <Route path='/profile' render={ () => <Profile /> }/>
                <Route path='/message' render={ () => <DialogsContainer /> }/>
            </div>
        </BrowserRouter>
    );
}

export default Body