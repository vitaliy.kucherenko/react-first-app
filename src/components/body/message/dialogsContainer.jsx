import React from 'react'
import {sendMessageCreator, updateMessageTextCreator} from "../../../data/dialogs-reducer";
import Dialogs from "./dialogs";
import {connect} from "react-redux";

// const DialogsContainer = (props) => {
//     let state = props.store.getState();
//     let containerSendMessageClick = () => {
//         props.store.dispatch(sendMessageCreator());
//     }
//     let containerUpdateMessageText = (text) => {
//         props.store.dispatch(updateMessageTextCreator(text));
//
//     }
//     return (
//         <Dialogs sendMessageClick={containerSendMessageClick} updateMessageText={containerUpdateMessageText}
//         dialogsPage={state.dialogsPage}/>
//         )
// }

let mapStateToProps = (state) => {
    return {
        dialogsPage: state.dialogsPage
    }
}

// let mapDispatchToProps = (dispatch) => {
//     let containerSendMessageClick = () => {
//         dispatch(sendMessageCreator());
//     }
//     let containerUpdateMessageText = (text) => {
//         dispatch(updateMessageTextCreator(text));
//     }
//     return {
//         sendMessageClick: containerSendMessageClick,
//         updateMessageText: containerUpdateMessageText
//     }
// }

let mapDispatchToProps = (dispatch) => {
    return {
        sendMessageClick: () => {
            dispatch(sendMessageCreator());
        },
        updateMessageText: (text) => {
            dispatch(updateMessageTextCreator(text));
        }
    }
}

let DialogsContainer = connect(mapStateToProps, mapDispatchToProps)(Dialogs);

export default DialogsContainer