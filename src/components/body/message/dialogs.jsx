import React from 'react'
import Styles from './dialogs.module.css'
import Message from "./message/message";
import DialogItem from "./dialogitem/dialogs";

const Dialogs = (props) => {
    let state = props.dialogsPage
    let dialogItem = state.dialogs.map(data => <DialogItem name={data.name} name_id={data.id} key={data.id}/>);
    let messageItem = state.messages.map(m => <Message message={m.message} key={m.id}/>);
    let newMessageText = state.newMessageText;

    let onSendMessageClick = () => {
        props.sendMessageClick();
    }
    let onUpdateMessageText = (e) => {
        let text = e.target.value;
        props.updateMessageText(text);

    }
    return (
        <div className={Styles.dialogs}>
            <div className={Styles.dialogsItem}>
                {dialogItem}
            </div>

            <div className={Styles.messages}>
                <div>{messageItem}</div>
                <div>
                    <div>
                        <textarea value={newMessageText} onChange={onUpdateMessageText} placeholder='Enter message'></textarea>
                    </div>
                    <div>
                        <button onClick={onSendMessageClick}>SEND</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dialogs