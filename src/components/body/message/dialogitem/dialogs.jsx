import React from 'react'
import Styles from './../dialogs.module.css'
import {NavLink} from "react-router-dom";

const DialogItem = (props) => {
    let path = '/message/' + props.name_id
    return (
        <div className={Styles.dialog}>
            <NavLink to={path}>{props.name}</NavLink>
        </div>
    )
}



export default DialogItem