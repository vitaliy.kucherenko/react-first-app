import React from 'react'
import ProfileInfo from "./profileinfo/profileinfo";
import PostsContainer from "./Post/postsConteiner";

const Profile = () => {
    return (
        <div>
            <ProfileInfo />
            <PostsContainer />
        </div>
    );
}

export default Profile