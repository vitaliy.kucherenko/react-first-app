import React from 'react'
import {addPostActionCreator, updateNewPostTextActionCreator} from "../../../../data/profile-reducer";
import Posts from "./post";
import {connect} from "react-redux";

// const PostsContainer = (props) => {
//     let state = props.store.getState();
//     const containerAddPost = () => {
//         props.store.dispatch(addPostActionCreator());
//     };
//
//     const containerChangePostText = (text) => {
//         props.store.dispatch(updateNewPostTextActionCreator(text))
//     }
//
//     return (
//         <Posts addPost={containerAddPost} changePostText={containerChangePostText}
//                newPostText={state.profilePage.text_post} posts={state.profilePage.posts} />
//     )
// }

let mapStateToProps = (state) => {
    return {
        newPostText: state.profilePage.text_post,
        posts: state.profilePage.posts
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        addPost: () => {
            dispatch(addPostActionCreator());
        },
        changePostText: (text) => {
            dispatch(updateNewPostTextActionCreator(text))
        }
    }
}

let PostsContainer = connect(mapStateToProps, mapDispatchToProps)(Posts)

export default PostsContainer