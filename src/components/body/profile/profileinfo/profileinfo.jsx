import React from 'react'
import Style from './profileinfo.module.css'

const ProfileInfo = (props) => {
    return (
        <div className={Style.profileInfo}>
            <div className=''>
                <img className={Style.avatar} src="https://i.pinimg.com/736x/2d/dc/25/2ddc25914e2ae0db5311ffa41781dda1.jpg" alt=""/>
            </div>
            <div className=''>info</div>
        </div>
    )
}

export default ProfileInfo