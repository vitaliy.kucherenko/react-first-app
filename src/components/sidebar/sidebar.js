import React from 'react'
import Styles from './sidebar.module.css';
import {NavLink} from "react-router-dom";

const Sidebar = () => {
    return (
        <div className={Styles.sidebar_base}>
            <div className={Styles.sidebar_item}>
                <NavLink to="/profile" activeClassName={Styles.ActiveLink}>Profile</NavLink>
            </div>
            <div className={Styles.sidebar_item}>
                <NavLink to="/message" activeClassName={Styles.ActiveLink}>Message</NavLink>
            </div>
        </div>
    );
}

export default Sidebar